package main

import (
	"fmt"
)

func twofer(whoever string) string {
	if whoever == "" {
		whoever = "you"
	}

	return fmt.Sprintf("One for %s, one for me.", whoever)
}

func main() {
	var shareit = twofer("you")
	fmt.Println(shareit)
}
